module ActiveRecord::ConnectionAdapters
  TableDefinition.class_eval do
    def deleted_at(*args)
      options = args.extract_options!
      column(:deleted_at, :datetime, options.reverse_merge({ comment: 'レコード削除日時' }))
    end
  end
end
