require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  shared_examples "200 test" do |controller|
    it '成功すること' do
      expect(response).to have_http_status(200)
    end

    it "#{controller}ビューをレンダーすること" do
      expect(response).to render_template(controller)
    end
  end

  describe "GET #index" do
    before { get :index }

    it_behaves_like '200 test', :index
  end
end
